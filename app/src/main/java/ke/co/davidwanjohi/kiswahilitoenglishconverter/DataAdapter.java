package ke.co.davidwanjohi.kiswahilitoenglishconverter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataAdapterViewHolder> {

    Context context;
    List<Language> languages;
    public DataAdapter(Context c, List<Language> languageList){
        context=c;
        languages=languageList;
    }
    @NonNull
    @Override
    public DataAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.single_data,viewGroup,false);
        return  new DataAdapterViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull DataAdapterViewHolder holder, int i) {

        Language language=languages.get(i);

        holder.textData.setText(language.kiswahili);

    }

    public void updateData(List<Language> updateData){

        this.languages=updateData;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return languages.size();
    }

    public class DataAdapterViewHolder extends RecyclerView.ViewHolder{

        TextView textData;
        public DataAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            textData=itemView.findViewById(R.id.text_word);
        }
    }
}
