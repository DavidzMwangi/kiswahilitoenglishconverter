package ke.co.davidwanjohi.kiswahilitoenglishconverter;


import com.activeandroid.ActiveAndroid;
import com.activeandroid.app.Application;

public class KiswahiliToEnglishApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ActiveAndroid.initialize(this);

    }
}
