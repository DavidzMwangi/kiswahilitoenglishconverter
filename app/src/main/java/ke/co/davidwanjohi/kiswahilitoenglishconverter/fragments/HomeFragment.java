package ke.co.davidwanjohi.kiswahilitoenglishconverter.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

import ke.co.davidwanjohi.kiswahilitoenglishconverter.DataAdapter;
import ke.co.davidwanjohi.kiswahilitoenglishconverter.Language;
import ke.co.davidwanjohi.kiswahilitoenglishconverter.R;

public class HomeFragment  extends Fragment {

    EditText enteredText;
    Button submitBtn;
    RecyclerView recyclerView;
    DataAdapter dataAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragement,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        enteredText=view.findViewById(R.id.entered_text);
        submitBtn=view.findViewById(R.id.submit_data);
        recyclerView=view.findViewById(R.id.recycler_view);



        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        dataAdapter=new DataAdapter(getActivity(),new ArrayList<Language>());
        recyclerView.setAdapter(dataAdapter);


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enteredText.getText().toString()!=null){

//query the data//SQLite.select().from(Medication.class)
//                        .where(Medication_Table.completed.eq(true)).queryList();
                    List<Language> languageList=new Select().from(Language.class).where("english=?",enteredText.getText().toString()).execute();
                    dataAdapter.updateData(languageList);

                    List<Language> erer=new Select().from(Language.class).execute();
                    Log.e("sdsd",""+languageList.size());
                }else{
                    return;
                }
            }
        });





    }
}
